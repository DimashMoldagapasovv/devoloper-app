import 'dart:async';
import 'dart:io';

import 'package:dvapp/models/order.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
// import 'package:sqlite_demo/ClientModel.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we in stantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "TestDB.db");
    return await openDatabase(path, version: 1, onOpen: (db) {}, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Orders ("
          "id INTEGER PRIMARY KEY,"
          "order_id INTEGER,"
          "basket TEXT,"
          "store_name TEXT,"
          "store_id INTEGER,"
          "store_address TEXT,"
          "total_cost TEXT,"
          "total_returns_cost TEXT,"
          "counterparty_name TEXT,"
          "delivery_at TEXT,"
          "status INTEGER,"
          "bonus_game_sum INTEGER"
          ")");
    });
  }

  newOrder(order) async {
    final db = await database;
    var raw = await db.rawInsert(
        "INSERT OR REPLACE INTO Orders (id,order_id,basket,store_name,store_id,store_address,total_cost,total_returns_cost,counterparty_name,"
        "delivery_at,status,bonus_game_sum)"
        " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
        [
          order['id'],
          order['order_id'],
          order['basket'],
          order['store_name'],
          order['store_id'],
          order['store_address'],
          order['total_cost'],
          order['total_returns_cost'],
          order['counterparty_name'],
          order['delivery_at'],
          order['status'],
          order['bonus_game_sum'],
        ]);
    return raw;
  }

  updateOrderStatus({int id, int status}) async {
    final db = await database;
    await db.rawUpdate('UPDATE Orders SET status = ? WHERE order_id = ?', [status, id]);
  }

  allOrders() async {
    final db = await database;
    var res = await db.query("Orders");
    List<Order> products = [];
    products = res.isNotEmpty ? res.map((c) => Order.fromMap(c)).toList() : [];
    return products;
  }

  getOrdersByStatusAndDate(int status) async {
    DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String date = formatter.format(DateTime.now());

    final db = await database;
    // var res = await db.rawQuery("SELECT * FROM Orders where status=? AND delivery_at =?", [status, '22-07-2021']);
    var res = await db.rawQuery("SELECT * FROM Orders where status=?", [status]);
    List<Order> orders = [];
    orders = res.isNotEmpty ? res.map((c) => Order.fromMap(c)).toList() : [];
    return orders;
  }

  getCounterparties(int status) async {
    final db = await database;
    return await db.rawQuery(
      "SELECT DISTINCT Orders.counterparty_name "
      "FROM Orders "
      "WHERE Orders.status == ? "
      "ORDER BY Orders.id DESC ",
      [status],
    );
  }

  getOrdersByCounterparties(String counterpartyName, int status) async {
    DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String date = formatter.format(DateTime.now());

    final db = await database;
    // var res = await db.rawQuery("SELECT * FROM Orders where status=? AND delivery_at =?", [status, '22-07-2021']);
    var res = await db.rawQuery("SELECT * FROM Orders WHERE status=? AND counterparty_name =? AND delivery_at =?", [status, counterpartyName, date]);
    List<Order> orders = [];
    orders = res.isNotEmpty ? res.map((c) => Order.fromMap(c)).toList() : [];
    return orders;
  }

  getOrdersByCounterpartyPrice(String counterpartyName) async {
    final db = await database;
    DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String date = formatter.format(DateTime.now());
    return db.rawQuery("SELECT SUM(total_cost) FROM Orders WHERE status = ? AND counterparty_name=? AND delivery_at =?", [3, counterpartyName, date]);
  }

  getOrdersByCounterpartyReturnPrice(String counterpartyName) async {
    final db = await database;
    DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String date = formatter.format(DateTime.now());
    return db.rawQuery(
        "SELECT SUM(total_returns_cost) FROM Orders WHERE status = ? AND counterparty_name=? AND delivery_at =?", [3, counterpartyName, date]);
  }

  getOrdersBonusGameSumByCounterpartyName(String counterpartyName) async {
    final db = await database;
    DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String date = formatter.format(DateTime.now());
    return db
        .rawQuery("SELECT SUM(bonus_game_sum) FROM Orders WHERE status = ? AND counterparty_name=? AND delivery_at =?", [3, counterpartyName, date]);
  }

  getOrderBonusGameSum() async {
    final db = await database;
    DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String date = formatter.format(DateTime.now());
    return db.rawQuery("SELECT SUM(bonus_game_sum) FROM Orders WHERE status = ? AND delivery_at =?", [3, date]);
  }

  getOrdersPrice() async {
    final db = await database;
    DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String date = formatter.format(DateTime.now());
    return db.rawQuery("SELECT SUM(total_cost) FROM Orders WHERE status = ? AND delivery_at =?", [3, date]);
  }
}
