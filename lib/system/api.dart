import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Api {
  static const API_URL = 'https://dev.boszhan.kz/api/';
  Dio dio = new Dio();

  login(String email, String password) async {
    dio.options.headers['content-Type'] = 'application/json';
    try {
      var response = await dio.post("${API_URL}auth/login", data: {
        "email": email,
        "password": password,
      });

      return response.data['data'];
    } catch (e) {
      if (e is DioError) {
        print("e.message get sad ccccccccc12313");
        print(e.response);
        print(e.error);
        print(e.message);
      }

      return null;
    }
  }

  getCurrentOrders() async {
    SharedPreferences store = await SharedPreferences.getInstance();

    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer ${store.getString('user_token')}";
    try {
      var response = await dio.get("${API_URL}delivery-order");
      print("123213response.data['data']");
      print(response.data);
      print("response.data['data']");

      return response.data['data'];
    } catch (e) {
      if (e is DioError) {
        print("e.message get sad ccccccccc12313");
        print(e.response);
        print(e.error);
        print(e.message);
      }

      return null;
    }
  }

  getOrdersByFilter({String filter}) async {
    SharedPreferences store = await SharedPreferences.getInstance();

    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer ${store.getString('user_token')}";
    try {
      var response = await dio.get("${API_URL}delivery-order$filter");
      return response.data["data"];
    } catch (e) {
      if (e is DioError) {
        print("e.message get sad ccccccccc12313");
        print(e.response);
        print(e.error);
        print(e.message);
      }

      return null;
    }
  }

  updateOrders({int orderId, int storeId}) async {
    SharedPreferences store = await SharedPreferences.getInstance();
    var editedBasket = store.getString("edited:basket:$orderId");
    var basket = editedBasket != null ? editedBasket : store.getString("basket:$orderId");
    dio.options.headers['Accept'] = 'application/json';
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer ${store.getString('user_token')}";
    try {
      var response = await dio.post(
        "${API_URL}order/update-for-store",
        data: {"order_id": orderId, "basket": jsonDecode(basket), "store_id": storeId},
        options: Options(
          followRedirects: true,
        ),
      );
      print("sad test response.data");
      print(response.data);
      return response.data;
    } catch (e) {
      if (e is DioError) {
        print("e.message get sad updateOrders");
        print(e.response);
        print(e.error);
        print(e.message);
      }
    }
  }

  updateOrderStatus(int orderId, int status) async {
    SharedPreferences store = await SharedPreferences.getInstance();
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers['Accept'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer ${store.getString('user_token')}";
    try {
      var response = await dio.post(
        "${API_URL}delivery-order/$orderId/change-status",
        data: {"status": status},
        options: Options(
          followRedirects: true,
        ),
      );
    } catch (e) {}
  }

  sendBezNal(orderCost, number) async {
    SharedPreferences store = await SharedPreferences.getInstance();
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers['Accept'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer ${store.getString('user_token')}";
    try {
      var response = await dio.post(
        "${API_URL}telegram-bot/driver-send-message",
        data: {"phone_number": "$number", "order_cost": "$orderCost"},
        options: Options(
          followRedirects: true,
        ),
      );
    } catch (e) {
      if (e is DioError) {
        print("e.message get sad sendBezNal");
        print(e.response);
        print(e.error);
        print(e.message);
      }
    }
  }
}
