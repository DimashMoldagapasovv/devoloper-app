import 'package:dvapp/pages/login_page.dart';
import 'package:dvapp/pages/order_types_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  SharedPreferences.setMockInitialValues({});
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        textTheme: TextTheme(
          bodyText1: TextStyle(fontSize: 20.0),
          bodyText2: TextStyle(fontSize: 16.0),
          button: TextStyle(fontSize: 16.0),
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(
          future: SharedPreferences.getInstance(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              snapshot.data.getString('user_token');
              print("sad coder test snapshot has data");
              print(snapshot.data.getString('user_token'));

              if (snapshot.data.getString("user_token") != null) {
                return OrderTypePage();
              }
              return LoginPage();
            } else {
              return LoginPage();
            }
          }),
    );
  }
}
