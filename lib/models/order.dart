import 'dart:convert';

Order brandFromJson(String str) {
  final jsonData = json.decode(str);
  return Order.fromMap(jsonData);
}

class Order {
  int id;
  int order_id;
  String basket;
  String store_name;
  int store_id;
  String store_address;
  String total_cost;
  String total_returns_cost;
  String counterparty_name;
  String delivery_at;
  int status;
  int bonus_game_sum;

  Order({
    this.id,
    this.order_id,
    this.basket,
    this.store_name,
    this.store_id,
    this.store_address,
    this.total_cost,
    this.total_returns_cost,
    this.counterparty_name,
    this.delivery_at,
    this.status,
    this.bonus_game_sum,
  });

  factory Order.fromMap(Map<String, dynamic> json) => Order(
        id: json["id"],
        order_id: json["order_id"],
        basket: json["basket"],
        store_name: json["store_name"],
        store_id: json["store_id"],
        store_address: json["store_address"],
        total_cost: json["total_cost"],
        total_returns_cost: json["total_returns_cost"],
        counterparty_name: json["counterparty_name"],
        delivery_at: json["delivery_at"],
        status: json["status"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "order_id": order_id,
        "basket": basket,
        "store_name": store_name,
        "store_id": store_id,
        "store_address": store_address,
        "total_cost": total_cost,
        "total_returns_cost": total_returns_cost,
        "counterparty_name": counterparty_name,
        "delivery_at": delivery_at,
        "status": status,
        "bonus_game_sum": bonus_game_sum,
      };
}

String brandToJson(Order data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}
