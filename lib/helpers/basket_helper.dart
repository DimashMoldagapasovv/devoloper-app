import 'dart:convert';

import 'package:dvapp/models/order.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BasketHelper {
  convertBasket(List<dynamic> basket) {
    basket.forEach((basketProduct) {
      basketProduct['collected_count'] = 0;
    });
    return basket;
  }

  void updateCountBasket(int barCode, count, int orderId) async {
    SharedPreferences store = await SharedPreferences.getInstance();
    List<dynamic> basket = jsonDecode(store.getString("basket:$orderId"));
    basket.forEach((product) {
      product['collected_count'] += count;
    });
  }

  getTotalCost(int orderId) async {
    SharedPreferences storage = await SharedPreferences.getInstance();

    var basket =
        storage.getString("edited:basket:$orderId") != null ? storage.getString("edited:basket:$orderId") : storage.getString("basket:$orderId");

    List<dynamic> products = jsonDecode(basket);
    double allPrice = 0.0;
    products.forEach((product) {
      if (product['type'] == 0) {
        allPrice += double.parse(product['price'].toString()) * double.parse(product['count'].toString());
      } else {
        allPrice -= double.parse(product['price'].toString()) * double.parse(product['count'].toString());
      }
    });
    return allPrice.ceil().toString();
  }

  getTotalCount(int orderId) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    var basket =
        storage.getString("edited:basket:$orderId") != null ? storage.getString("edited:basket:$orderId") : storage.getString("basket:$orderId");
    List<dynamic> products = jsonDecode(basket);
    return products.length;
  }

  getOrdersRnkPrice(List<Order> orders) async {
    SharedPreferences store = await SharedPreferences.getInstance();
    double totalPrice = 0;
    await orders.forEach((order) async {
      if (store.getString('edited:basket:${order.order_id}') != null) {
        var cost = await this.getTotalCost(order.order_id);
        totalPrice += int.parse(cost);
      } else {
        totalPrice += double.parse(order.total_cost);
      }
    });
    return totalPrice.toInt();
  }
}
