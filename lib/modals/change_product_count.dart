import 'dart:async';
import 'dart:convert';

import 'package:dvapp/pages/view_order.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangeProductCount extends StatelessWidget {
  final orderId;
  final productId;

  ChangeProductCount({Key key, this.orderId, this.productId}) : super(key: key);
  TextEditingController countController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.4),
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
          ),
          width: MediaQuery.of(context).size.width * 0.94,
          margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.18, left: MediaQuery.of(context).size.width * 0.03),
          height: MediaQuery.of(context).size.height * 0.65,
          child: Container(
              child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 800),
                child: IconButton(
                  icon: Icon(Icons.close, color: Colors.red,),
                  onPressed:(){Navigator.pop(context);}
                ),
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Text(
                    "Изменить количество",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.5,
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                child: TextField(
                  controller: countController,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                  maxLength: 6,
                  keyboardType: TextInputType.numberWithOptions(),
                  decoration: InputDecoration(
                      hintStyle: TextStyle(
                        color: Colors.red,
                      ),
                      fillColor: Colors.red,
                      border: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                      // hintText: 'пароль',
                      helperText: 'Введите количество',
                      labelText: 'Количество',
                      labelStyle: TextStyle(color: Colors.red),
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
                      prefixIcon: const Icon(
                        Icons.wifi_protected_setup,
                        color: Colors.red,
                      ),
                      prefixText: '',
                      suffixStyle: const TextStyle(color: Colors.yellow)),
                  onEditingComplete: () async {},
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        color: Colors.grey,
                        child: Container(
                          margin: EdgeInsets.all(20),
                          child: Text(
                            "отмена",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    RaisedButton(
                      onPressed: () async {
                        if (countController.text != null) {
                          // получить корзину
                          SharedPreferences store = await SharedPreferences.getInstance();
                          print("sad coder orderId");
                          print(store.getString("basket:${orderId}"));

                          var editedBasket = store.getString("edited:basket:${orderId}");

                          List<dynamic> basket = editedBasket == null ? jsonDecode(store.getString("basket:${orderId}")) : jsonDecode(editedBasket);
                          // потом перебрать корзину
                          basket.forEach((element) {
                            if (element['product_id'] == productId) {
                              element['count'] = countController.text;
                              element['all_price'] = double.parse(countController.text) * element['price'];
                            }
                          });
                          String editBasket = jsonEncode(basket);
                          store.setString("edited:basket:$orderId", editBasket);
                          Timer(Duration(seconds: 0), () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewOrder(
                                          basket: editBasket,
                                          orderId: orderId,
                                        )));
                          });
                        }
                      },
                      color: Colors.green,
                      child: Container(
                        margin: EdgeInsets.all(20),
                        child: Text(
                          "изменить",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                width: MediaQuery.of(context).size.width * 0.3,
                child: Container(
                  child: RaisedButton(
                      padding: EdgeInsets.all(20),
                      child: Container(
                        margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04),
                        child: Row(
                          children: [
                            Text(
                              "Удалить",
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            Icon(
                              Icons.delete_forever,
                              size: 20,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                      color: Colors.red,
                      onPressed: () async {
                        SharedPreferences store = await SharedPreferences.getInstance();

                        var editedBasket = store.getString("edited:basket:${orderId}");

                        List<dynamic> basket = editedBasket == null ? jsonDecode(store.getString("basket:${orderId}")) : jsonDecode(editedBasket);
                        // потом перебрать корзину
                        basket.removeWhere((element) => element['product_id'] == productId);

                        String editBasket = jsonEncode(basket);
                        store.setString("edited:basket:$orderId", editBasket);
                        Timer(Duration(seconds: 0), () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ViewOrder(
                                        basket: editBasket,
                                        orderId: orderId,
                                      )));
                        });
                      }),
                ),
              ),
            ],
          )),
        ),
      ),
    );
  }
}
