import 'package:dvapp/helpers/basket_helper.dart';
import 'package:dvapp/models/order.dart';
import 'package:dvapp/pages/day_print.dart';
import 'package:dvapp/system/api.dart';
import 'package:dvapp/system/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'pages/view_order.dart';

class DeliveredOrders extends StatefulWidget {
  final orderId;

  const DeliveredOrders({Key key, this.orderId}) : super(key: key);

  // final orderId;

  @override
  _DeliveredOrdersState createState() => _DeliveredOrdersState();
}

class _DeliveredOrdersState extends State<DeliveredOrders> {
  Future orders;

  @override
  void initState() {
    orders = Api().getOrdersByFilter(filter: "?filter[status]=3");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Выполненные заказы"),
          centerTitle: true,
          backgroundColor: Colors.red,
        ),
        body: FutureBuilder(
          future: DBProvider.db.getOrdersByStatusAndDate(
            3,
          ),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              print("sad coder test snap data");
              print(snapshot.data.runtimeType);
              return Column(children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                          onTap: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewOrder(
                                          basket: snapshot.data[index].basket,
                                          title: "просмотр заказа",
                                          basketCost: snapshot.data[index].total_cost,
                                        )));
                          },
                          child: Container(
                              margin: EdgeInsets.only(top: 10),
                              height: 50,
                              decoration: BoxDecoration(color: Colors.grey[300], borderRadius: BorderRadius.circular(20)),
                              child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                                Icon(
                                  Icons.location_on_outlined,
                                  color: Colors.grey,
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Text(
                                    "${snapshot.data[index].store_name} ${snapshot.data[index].store_address}",
                                    style: TextStyle(color: Colors.black),
                                  ),
                                )
                              ])));
                    },
                  ),
                ),
                // Center(
                //   child: Container(
                //     margin: EdgeInsets.only(bottom: 20),
                //     child: Builder(context) {
                //       Text("Общая сумма заказов ");
                //     }),
                //   ),
                Container(
                  child: RaisedButton.icon(
                    onPressed: () async {
                      var counterparties = await DBProvider.db.getCounterparties(3);

                      var firstCounterparty = await DBProvider.db.getOrdersByCounterparties(counterparties[0]['counterparty_name'], 3);
                      var secondCounterparty = await DBProvider.db.getOrdersByCounterparties(counterparties[1]['counterparty_name'], 3);

                      Map<String, List<Order>> ordersFilteredByCounterparty = {
                        counterparties[0]['counterparty_name']: firstCounterparty,
                        counterparties[1]['counterparty_name']: secondCounterparty
                      };

                      // сумма заказа
                      var firstCounterpartyOrderPrice =
                          (await DBProvider.db.getOrdersByCounterpartyPrice(counterparties[0]['counterparty_name']))[0]['SUM(total_cost)'];
                      var secondCounterpartyOrderPrice =
                          (await DBProvider.db.getOrdersByCounterpartyPrice(counterparties[1]['counterparty_name']))[0]['SUM(total_cost)'];

                      Map<String, int> orderPricesByCounterparty = {
                        counterparties[0]['counterparty_name']: firstCounterpartyOrderPrice,
                        counterparties[1]['counterparty_name']: secondCounterpartyOrderPrice,
                      };

                      var firstCounterpartyOrderReturnPrice = (await DBProvider.db
                          .getOrdersByCounterpartyReturnPrice(counterparties[0]['counterparty_name']))[0]['SUM(total_returns_cost)'];

                      var secondCounterpartyOrderReturnPrice = (await DBProvider.db
                          .getOrdersByCounterpartyReturnPrice(counterparties[1]['counterparty_name']))[0]['SUM(total_returns_cost)'];

                      var firstCounterpartyOrderBonusGameSum = (await DBProvider.db
                          .getOrdersBonusGameSumByCounterpartyName(counterparties[0]['counterparty_name']))[0]['SUM(bonus_game_sum)'];
                      var secondCounterpartyOrderBonusGameSum = (await DBProvider.db
                          .getOrdersBonusGameSumByCounterpartyName(counterparties[1]['counterparty_name']))[0]['SUM(bonus_game_sum)'];

                      Map<String, int> orderReturnPricesByCounterparty = {
                        counterparties[0]['counterparty_name']: firstCounterpartyOrderReturnPrice.ceil(),
                        counterparties[1]['counterparty_name']: secondCounterpartyOrderReturnPrice.ceil(),
                      };

                      Map<String, int> bonusGameSumByCounterparty = {
                        counterparties[0]['counterparty_name']: firstCounterpartyOrderBonusGameSum,
                        counterparties[1]['counterparty_name']: secondCounterpartyOrderBonusGameSum,
                      };
                      SharedPreferences storage = await SharedPreferences.getInstance();
                      List<TableRow> allOrders = [];

                      List<TableCell> fields = [];
                      var firstCounterpartyOrdersSum =
                          await BasketHelper().getOrdersRnkPrice(ordersFilteredByCounterparty[counterparties[0]['counterparty_name']]);
                      var secondCounterpartyOrdersSum =
                          await BasketHelper().getOrdersRnkPrice(ordersFilteredByCounterparty[counterparties[1]['counterparty_name']]);

                      var orderBonusGameSum = (await DBProvider.db.getOrderBonusGameSum())[0]['SUM(bonus_game_sum)'];
                      var ordersPrice = (await DBProvider.db.getOrdersPrice())[0]['SUM(total_cost)'];

                      List ordersGroped = [];
                      ordersGroped.add({
                        "counterpartyOrdersSum": firstCounterpartyOrdersSum,
                        "counterpartyOrderPrice": firstCounterpartyOrderPrice,
                        "counterpartyBonusGameSum": firstCounterpartyOrderBonusGameSum ?? 0,
                        "counterpartyOrderReturnPrice": firstCounterpartyOrderReturnPrice,
                        "counterpartyName": counterparties[0]['counterparty_name'],
                        "type": "counterpartyStat"
                      });
                      firstCounterparty.forEach((order) {
                        ordersGroped.add(order);
                      });
                      ordersGroped.add({
                        "counterpartyOrdersSum": secondCounterpartyOrdersSum,
                        "counterpartyOrderPrice": secondCounterpartyOrderPrice,
                        "counterpartyBonusGameSum": secondCounterpartyOrderBonusGameSum ?? 0,
                        "counterpartyOrderReturnPrice": secondCounterpartyOrderReturnPrice,
                        "counterpartyName": counterparties[1]['counterparty_name'],
                        "type": "counterpartyStat"
                      });
                      secondCounterparty.forEach((order) {
                        ordersGroped.add(order);
                      });

                      ordersGroped.add({
                        "type": "all_statistic",
                        "driver_name": storage.getString("user_full_name"),
                        "ordersPrice": ordersPrice,
                        "ordersRnkSum": firstCounterpartyOrdersSum + secondCounterpartyOrdersSum,
                        "returnSum": firstCounterpartyOrderReturnPrice + secondCounterpartyOrderReturnPrice,
                        "bonusGameSum": firstCounterpartyOrderBonusGameSum + secondCounterpartyOrderBonusGameSum
                      });

                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DayPrint(
                                    store: storage,
                                    orders: snapshot.data,
                                    ordersGroped: ordersGroped,
                                    firstCounterpartyOrders: ordersFilteredByCounterparty[counterparties[0]['counterparty_name']],
                                    secondCounterpartyOrders: ordersFilteredByCounterparty[counterparties[1]['counterparty_name']],
                                    firstCounterpartyName: counterparties[0]['counterparty_name'],
                                    secondCounterpartyName: counterparties[1]['counterparty_name'],
                                    firstCounterpartyOrderPrice: firstCounterpartyOrderPrice,
                                    secondCounterpartyOrderPrice: secondCounterpartyOrderPrice,
                                    firstCounterpartyOrdersSum: firstCounterpartyOrdersSum,
                                    secondCounterpartyOrdersSum: secondCounterpartyOrdersSum,
                                    firstCounterpartyReturnSum: firstCounterpartyOrderReturnPrice,
                                    secondCounterpartyReturnSum: secondCounterpartyOrderReturnPrice,
                                    storage: storage,
                                    allOrdersTable: allOrders,
                                    orderBonusGameSum: orderBonusGameSum,
                                    userName: storage.getString("user_full_name"),
                                    ordersPrice: ordersPrice,
                                  )));
                    },
                    color: Colors.blue,
                    icon: Icon(
                      Icons.print,
                      color: Colors.white,
                    ),
                    label: Text('Распечатать', style: TextStyle(color: Colors.white)),
                  ),
                ),
                Center(
                    child: Container(
                  color: Colors.purple,
                  margin: EdgeInsets.only(top: 30),
                  child: FutureBuilder(
                    future: BasketHelper().getTotalCost(this.widget.orderId),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return Text(
                          "Общая сумма заказа ${snapshot.data}",
                          style: TextStyle(color: Colors.black),
                        );
                      }
                      return Container();
                    },
                  ),
                ))
              ]);
            }
            return Container();
          },
        ));
  }
}
