import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';

class PrintPage extends StatefulWidget {
  final List<dynamic> basket;
  final String basketPrice;

  const PrintPage({Key key, this.basket, this.basketPrice}) : super(key: key);

  @override
  _PrintPageState createState() => _PrintPageState();
}

class _PrintPageState extends State<PrintPage> {
  GlobalKey _globalKey = new GlobalKey();
  ScreenshotController screenshotController = ScreenshotController();
  List<TableRow> products = [];

  @override
  void initState() {
    int number = 0;
    this.widget.basket.forEach((productFields) {
      List<TableCell> fields = [];
      fields.add(TableCell(
        child: TableCell(
          child: Text('${(++number).toString()}'),
          verticalAlignment: TableCellVerticalAlignment.bottom,
        ),
      ));
      fields.add(TableCell(
        child: TableCell(
          child: Text('${productFields['name_1c'] ?? productFields['name']}'),
          verticalAlignment: TableCellVerticalAlignment.bottom,
        ),
      ));
      fields.add(TableCell(
        child: TableCell(
          child: Text('1'),
          verticalAlignment: TableCellVerticalAlignment.bottom,
        ),
      ));
      fields.add(TableCell(
        child: TableCell(
          child: Text('${productFields['measure_id'] == 1 ? "шт" : "кг"}'),
          verticalAlignment: TableCellVerticalAlignment.bottom,
        ),
      ));
      fields.add(TableCell(
        child: TableCell(
          child: Table(
            border: TableBorder.all(color: Colors.black, width: 2, style: BorderStyle.solid),
            children: [
              TableRow(children: [
                TableCell(child: Center(child: Text('${productFields['count']}'))),
                TableCell(child: Center(child: Text('${productFields['count']}'))),
              ])
            ],
          ),
          verticalAlignment: TableCellVerticalAlignment.bottom,
        ),
      ));
      fields.add(TableCell(
        child: TableCell(
          child: Text("${productFields['price'].toString()}"),
          verticalAlignment: TableCellVerticalAlignment.bottom,
        ),
      ));
      fields.add(TableCell(
        child: TableCell(
          child: Text('${productFields['price'].toString()}'),
          verticalAlignment: TableCellVerticalAlignment.bottom,
        ),
      ));
      fields.add(TableCell(
        child: TableCell(
          child: Text('${(productFields['price'] / 100 * 12).toStringAsFixed(2)}'),
          verticalAlignment: TableCellVerticalAlignment.bottom,
        ),
      ));
      products.add(TableRow(
        children: [...fields],
      ));
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setPreferredOrientations([
    //   DeviceOrientation.landscapeRight,
    // ]);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Распечатка накладной'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(1.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Screenshot(
                controller: screenshotController,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerRight,
                        child: Column(
                          children: [
                            Container(
                              child: Text("Приложение 26"),
                            ),
                            Container(
                              child: Text("к приказу Министра финансов Республики"),
                            ),
                            Container(
                              child: Text("Казахстан"),
                            ),
                            Container(
                              child: Text("от 20 декабря 2012 г. № 562"),
                            ),
                            Container(
                              child: Text("Форма З-2"),
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: Text(
                          "НАКЛАДНАЯ  НА ОТПУСК ЗАПАСОВ  НА СТОРОНУ",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                      Table(
                        border: TableBorder.all(color: Colors.black, width: 2, style: BorderStyle.solid),
                        children: [
                          TableRow(children: [
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Организация (индивидуальный предприниматель) - отправитель',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(
                                  child: Text(
                                'ТОО "Первомайские Деликатесы"',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                            TableCell(
                                child: Center(
                                    child: Text(
                              'ИИН/БИН',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                                child: Center(
                                    child: Text(
                              '130740008859',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                          ]),
                        ],
                      ),
                      Table(
                        border: TableBorder.all(color: Colors.black, width: 2, style: BorderStyle.solid),
                        children: [
                          TableRow(children: [
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Номер документа',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(
                                  child: Text(
                                'Дата составления',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                                child: Container(
                              color: Colors.grey,
                              child: Text(
                                '',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                            )),
                            TableCell(
                              child: Container(
                                color: Colors.grey,
                                child: Text(
                                  '',
                                  style: TextStyle(
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            ),
                          ]),
                        ],
                      ),
                      Table(
                        border: TableBorder.all(color: Colors.black, width: 2, style: BorderStyle.solid),
                        children: [
                          TableRow(children: [
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Организация (индивидуальный предприниматель) - отправитель',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(
                                  child: Text(
                                'Организация (индивидуальный предприниматель) - получатель',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Ответственный за поставку (Ф.И.О.)',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Транспортная организация',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Товарно-транспортная накладная (номер, дата)',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                          ]),
                          TableRow(children: [
                            TableCell(
                              child: Text(
                                'ТОО "Перво-майские Делика-тесы"',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                              verticalAlignment: TableCellVerticalAlignment.bottom,
                            ),
                            TableCell(
                              verticalAlignment: TableCellVerticalAlignment.middle,
                              child: Center(
                                  child: Text(
                                'Контрагент (Торговая точка)',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                            TableCell(
                                child: Center(
                                    child: Text(
                              "ФИО водителя",
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(child: Text('')),
                              verticalAlignment: TableCellVerticalAlignment.top,
                            ),
                            TableCell(
                              child: Center(child: Text('')),
                              verticalAlignment: TableCellVerticalAlignment.top,
                            ),
                          ]),
                        ],
                      ),
                      Table(
                        columnWidths: {
                          4: FlexColumnWidth(3),
                          1: FlexColumnWidth(4),
                          5: FlexColumnWidth(2),
                          6: FlexColumnWidth(3),
                          7: FlexColumnWidth(2),
                        },
                        border: TableBorder.all(color: Colors.black, width: 2, style: BorderStyle.solid),
                        children: [
                          TableRow(children: [
                            TableCell(child: Center(child: Text('№ п/п'))),
                            TableCell(
                              child: Center(child: Text('Наименование')),
                            ),
                            TableCell(child: Center(child: Text('Номенклатурный номер'))),
                            TableCell(child: Center(child: Text('Ед. изм.'))),
                            TableCell(
                                child: Table(
                              border: TableBorder.all(color: Colors.black, width: 2, style: BorderStyle.solid),
                              children: [
                                TableRow(children: [
                                  TableCell(child: Center(child: Text('Надлежит отпуску'))),
                                  TableCell(child: Center(child: Text('Отпу-щено'))),
                                ]),
                                TableRow(children: [
                                  TableCell(child: Center(child: Text('1'))),
                                  TableCell(child: Center(child: Text('1'))),
                                ])
                              ],
                            )),
                            TableCell(child: Center(child: Text('Цена, в KZT'))),
                            TableCell(child: Center(child: Text('Сумма с НДС, в KZT'))),
                            TableCell(child: Center(child: Text('Сумма НДС, в KZT'))),
                          ]),
                          ...products,
                          TableRow(children: [
                            TableCell(child: Center(child: Text(''))),
                            TableCell(
                              child: Center(child: Text('')),
                            ),
                            TableCell(child: Center(child: Text(''))),
                            TableCell(child: Center(child: Text(''))),
                            TableCell(child: Center(child: Text('Итого'))),
                            TableCell(child: Center(child: Text(this.widget.basketPrice))),
                            TableCell(child: Center(child: Text(this.widget.basketPrice))),
                            TableCell(child: Center(child: Text("sad"))),
                            // TableCell(child: Center(child: Text((int.parse(this.widget.basketPrice) / 100 * 12).toString()))),
                          ]),
                        ],
                      ),
                      Container(
                        alignment: Alignment.topLeft,
                        child: Text("Всего отпущено количество запасов (прописью): "),
                      ),
                      Container(
                        alignment: Alignment.topLeft,
                        child: Text("на сумму (прописью)"),
                      ),
                      Table(
                        border: TableBorder.all(color: Colors.black, width: 2, style: BorderStyle.solid),
                        children: [
                          TableRow(children: [
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Отпуск разрешил ',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(
                                  child: Text(
                                'По доверенности №    от  ___________',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                                child: Center(
                                    child: Text(
                              'ФИО, подпись',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(
                                  child: Text(
                                'выданной ______________',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Главный бухгалтер _________________',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(
                                  child: Text(
                                '',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                                child: Center(
                                    child: Text(
                              'М.П      ФИО Подпись',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(
                                  child: Text(
                                '',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                                child: Center(
                                    child: Text(
                              'Отпустил ___________________________',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ))),
                            TableCell(
                              child: Center(
                                  child: Text(
                                'Запасы получил ________________',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              )),
                            ),
                          ]),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text("Фио, подпись"),
                            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.15),
                          ),
                          Container(child: Text("Подпись, расшифровка подписи")),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Container(
                child: RaisedButton.icon(
                  color: Colors.purple,
                   icon: Icon(Icons.save_alt_rounded, color: Colors.white,),
                   label: Text("Сохранить в галерею", style: TextStyle(color: Colors.white),),
                    onPressed: () async {
                      screenshotController.capture(delay: Duration(milliseconds: 10), pixelRatio: 2).then((capturedImage) async {
                        print("sad coder capturedImage");
                        // print(pngBytes);
                        await _getStoragePermission();
                        var test = await Permission.storage.isGranted;
                        // await _getStoragePermission();

                        if (capturedImage.buffer.asUint8List() != null) {
                          // img.Image image = img.decodeImage(capturedImage);
                          // // var test1 = MediaQuery.of(context).size.width * 1.0;
                          // img.Image thumbnail = img.copyResize(image, width: 1200);

                          // print(img.encodePng(thumbnail));
                          Directory tempDir = await getTemporaryDirectory();
                          // File(tempDir.path + "/sad_coder-name.jpg").writeAsBytes(capturedImage);
                          final result = await ImageGallerySaver.saveImage(capturedImage, quality: 100);
                          print(result);
                          print("sad coder thumbnail");
                          print("sad test");
                        }
                      }).catchError((onError) {
                        print(onError);
                      });
                      // await _capturePng();
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  // Future<void> _capturePng() {
  //   return new Future.delayed(const Duration(milliseconds: 200), () async {
  //     RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject();
  //     ui.Image image = await boundary.toImage();
  //     ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
  //     Uint8List pngBytes = byteData.buffer.asUint8List();
  //     print(pngBytes);
  //     await _getStoragePermission();
  //     var test = await Permission.manageExternalStorage.isGranted;
  //     print(test);
  //
  //     final result = await ImageGallerySaver.saveImage(pngBytes, quality: 100, name: "t.png");
  //     print(result);
  //   });
  // }
  Future _capturePng() async {
    try {
      await _getStoragePermission();
      print('inside');
      RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject() as RenderRepaintBoundary;
      // if it needs repaint, we paint it.
      // if (boundary.debugNeedsPaint) {
      //   Timer(Duration(seconds: 1), () async => await _capturePng());
      //   return null;
      // }

      ui.Image image = await boundary.toImage();
      ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
      if (byteData != null) {
        final result = await ImageGallerySaver.saveImage(byteData.buffer.asUint8List(), quality: 100);
        print(result);
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future _getStoragePermission() async {
    if (await Permission.storage.request().isGranted) {
      //
    } else if (await Permission.storage.request().isPermanentlyDenied) {
      await openAppSettings();
    } else if (await Permission.storage.request().isDenied) {
      await openAppSettings();
    }
  }

// _saveScreen() async {
//   RenderRepaintBoundary boundary =
//   _globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
//   ui.Image image = await boundary.toImage();
//   ByteData? byteData = await (image.toByteData(format: ui.ImageByteFormat.png) as FutureOr<ByteData?>);
//   if (byteData != null) {
//     final result =
//     await ImageGallerySaver.saveImage(byteData.buffer.asUint8List());
//     print(result);
//   }
// }

}
