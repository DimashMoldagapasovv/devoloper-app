import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:dvapp/models/order.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DayPrint extends StatefulWidget {
  final orders;
  final SharedPreferences storage;
  final allOrdersTable;
  final userName;
  final orderBonusGameSum;
  final ordersPrice;
  final firstCounterpartyOrders;
  final secondCounterpartyOrders;
  final firstCounterpartyName;
  final secondCounterpartyName;
  final firstCounterpartyOrderPrice;
  final secondCounterpartyOrderPrice;
  final firstCounterpartyOrdersSum;
  final secondCounterpartyOrdersSum;
  final firstCounterpartyReturnSum;
  final secondCounterpartyReturnSum;
  final firstCounterpartyOrderBonusGameSum;
  final secondCounterpartyOrderBonusGameSum;
  final ordersGroped;
  final store;

  const DayPrint(
      {Key key,
      this.orders,
      this.storage,
      this.allOrdersTable,
      this.userName,
      this.orderBonusGameSum,
      this.ordersPrice,
      this.firstCounterpartyOrders,
      this.secondCounterpartyOrders,
      this.firstCounterpartyName,
      this.secondCounterpartyName,
      this.firstCounterpartyOrderPrice,
      this.secondCounterpartyOrderPrice,
      this.firstCounterpartyOrdersSum,
      this.secondCounterpartyOrdersSum,
      this.firstCounterpartyReturnSum,
      this.secondCounterpartyReturnSum,
      this.firstCounterpartyOrderBonusGameSum,
      this.secondCounterpartyOrderBonusGameSum,
      this.ordersGroped,
      this.store})
      : super(key: key);

  @override
  _DayPrintState createState() => _DayPrintState();
}

class _DayPrintState extends State<DayPrint> {
  GlobalKey _globalKey = new GlobalKey();
  ScreenshotController screenshotController = ScreenshotController();
  List<TableRow> allOrders = [];
  bool allowScreenshot;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
    ]);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text('Распечатка за день'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(1.0),
        child: SingleChildScrollView(
          child: Theme(
            data: Theme.of(context).copyWith(
              dividerColor: Colors.black,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Screenshot(
                  controller: screenshotController,
                  child: Container(
                    decoration: BoxDecoration(border: Border.all(color: Colors.black), color: Colors.white),
                    child: Column(
                      children: [
                        DataTable(
                            dividerThickness: 2,
                            dataTextStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
                            columnSpacing: 5,
                            columns: [
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "№ заказа",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "№ РНК",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "Контрагент",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "Торговая точка",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "Сумма заказа",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "Сумма РНК",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "Сумма возврата",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "Сумма выигрыша",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "Итого нал.",
                                  softWrap: true,
                                ),
                              )),
                              DataColumn(
                                  label: Container(
                                child: VerticalDivider(
                                  color: Colors.black,
                                ),
                              )),
                              DataColumn(
                                  label: Flexible(
                                child: Text(
                                  "Итого безнал.",
                                  softWrap: true,
                                ),
                              )),
                            ],
                            rows: this.widget.ordersGroped.map<DataRow>((order) {
                              if (order.runtimeType == Order) {
                                var editedOrderBasket = this.widget.store.getString("edited:basket:${order.order_id}");
                                var orderPrice = editedOrderBasket == null ? order.total_cost : 100;
                                if (editedOrderBasket != null) {
                                  List<dynamic> products = jsonDecode(editedOrderBasket);
                                  double allPrice = 0.0;
                                  products.forEach((product) {
                                    if (product['type'] == 0) {
                                      allPrice += product['price'] * double.parse(product['count'].toString());
                                    } else {
                                      allPrice -= product['price'] * double.parse(product['count'].toString());
                                    }
                                  });
                                  orderPrice = allPrice.toString();
                                }
                                var beznalPrice = this.widget.store.getString("beznal:${order.order_id}");
                                var nalPrice = this.widget.store.getString("nal:${order.order_id}");

                                return DataRow(cells: [
                                  DataCell(Text("${order.id}")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("${order.counterparty_name}")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("${order.store_name}")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("${order.total_cost}")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("$orderPrice")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("${order.total_returns_cost}")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("${order.bonus_game_sum ?? '0'}")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("${nalPrice == null ? '0' : nalPrice}")),
                                  DataCell(VerticalDivider(
                                    color: Colors.black,
                                    thickness: 2,
                                  )),
                                  DataCell(Text("${beznalPrice == null ? '0' : beznalPrice}")),
                                ]);
                              } else {
                                if (order['type'] == "counterpartyStat") {
                                  return DataRow(cells: [
                                    DataCell(Text("")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['counterpartyName']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['counterpartyOrderPrice']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['counterpartyOrdersSum']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['counterpartyOrderReturnPrice']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['counterpartyBonusGameSum'] ?? '0'}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("0")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("0")),
                                  ]);
                                } else if (order['type'] == "all_statistic") {
                                  return DataRow(cells: [
                                    DataCell(Text("Итого")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['driver_name']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['ordersRnkSum']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['ordersPrice']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['returnSum']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("${order['bonusGameSum']}")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("")),
                                    DataCell(VerticalDivider(
                                      color: Colors.black,
                                      thickness: 2,
                                    )),
                                    DataCell(Text("0")),
                                  ]);
                                }

                                //ordersGroped
                              }
                            }).toList()),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: RaisedButton(
                      child: Text("Создать изображение"),
                      onPressed: () async {
                        screenshotController.capture(delay: Duration(milliseconds: 10), pixelRatio: 2).then((capturedImage) async {
                          await _getStoragePermission();

                          if (capturedImage.buffer.asUint8List() != null) {
                            // img.Image image = img.decodeImage(capturedImage);
                            // // var test1 = MediaQuery.of(context).size.width * 1.0;
                            // img.Image thumbnail = img.copyResize(image, width: 1200);

                            // print(img.encodePng(thumbnail));
                            final result = await ImageGallerySaver.saveImage(capturedImage, quality: 100);
                          }
                        }).catchError((onError) {
                          print(onError);
                        });
                        // await _capturePng();
                      }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future _capturePng() async {
    try {
      await _getStoragePermission();
      print('inside');
      RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject() as RenderRepaintBoundary;
      // if it needs repaint, we paint it.
      // if (boundary.debugNeedsPaint) {
      //   Timer(Duration(seconds: 1), () async => await _capturePng());
      //   return null;
      // }

      ui.Image image = await boundary.toImage();
      ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
      if (byteData != null) {
        final result = await ImageGallerySaver.saveImage(byteData.buffer.asUint8List(), quality: 100);
        print(result);
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future _getStoragePermission() async {
    if (await Permission.storage.request().isGranted) {
      // setState(() {
      //   permissionGranted = true;
      // });
    } else if (await Permission.storage.request().isPermanentlyDenied) {
      await openAppSettings();
    } else if (await Permission.storage.request().isDenied) {
      // setState(() {
      //   permissionGranted = false;
      // });
    }
  }
}
