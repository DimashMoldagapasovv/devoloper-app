import 'dart:async';
import 'dart:convert';

import 'package:dvapp/pages/view_order.dart';
import 'package:dvapp/helpers/basket_helper.dart';
import 'package:dvapp/pages/print_page.dart';
import 'package:dvapp/system/database.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class CurrentOrders extends StatefulWidget {
  @override
  _CurrentOrdersState createState() => _CurrentOrdersState();
}

class _CurrentOrdersState extends State<CurrentOrders> {
  // late Future<List<dynamic>> orders;
  Future orders;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Текущие заказы"),
          centerTitle: true,
          backgroundColor: Colors.red,
        ),
        body: FutureBuilder(
            future: DBProvider.db.getOrdersByStatusAndDate(2),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                        onTap: () async {
                          SharedPreferences store = await SharedPreferences.getInstance();
                          //@TODO добавляем себе кастомный атрибут в массив количество уже собранных товаров (пока нету склада эта параша нужна)
                          if (store.getString("basket:${snapshot.data[index].order_id}") == null) {
                            store.setString("basket:${snapshot.data[index].order_id}", snapshot.data[index].basket);
                          }
                          var basket = store.getString("edited:basket:${snapshot.data[index].order_id}") != null
                              ? store.getString("edited:basket:${snapshot.data[index].order_id}")
                              : snapshot.data[index].basket;
                          Timer(Duration(seconds: 0), () {
                            _showOrderAction(
                              context: context,
                              basket: basket,
                              orderId: snapshot.data[index].order_id,
                              storeId: snapshot.data[index].store_id,
                            );
                          });
                        },
                        child: Container(
                            margin: EdgeInsets.only(top: 10),
                            height: 50,
                            decoration: BoxDecoration(color: Colors.grey[300], borderRadius: BorderRadius.circular(20)),
                            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                              Icon(
                                Icons.location_on_outlined,
                                color: Colors.grey,
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  // '${snapshot.data[index}',
                                  "Название магазина: ${snapshot.data[index].store_name},         Адресс: ${snapshot.data[index].store_address}",
                                  style: TextStyle(color: Colors.black),
                                ),
                              )
                            ])));
                  },
                );
              }
              return Center(
                child: Container(
                  child: CircularProgressIndicator(),
                ),
              );
            }));
  }

  _showOrderAction({context, basket, int orderId, int storeId, String basketCost}) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 290,
              child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(
                          'Что вы хотите сделать?',
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 20.0,
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.only(top: 60),
                      height: 50,
                      width: 395,
                      decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(3)),
                      // ignore: deprecated_member_use
                      child: FlatButton.icon(
                        onPressed: () {
                          Timer(Duration(seconds: 0), () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewOrder(
                                          basket: basket,
                                          orderId: orderId,
                                          storeId: storeId,
                                          basketCost: basketCost,
                                        )));
                          });
                        },
                        icon: Icon(Icons.open_in_new, color: Colors.white,),
                        label: Text('Открыть заказ', style: TextStyle(color: Colors.white),),
                      ),
                      // ignore: deprecated_member_use
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        height: 50,
                        width: 390,
                        decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.circular(3)),
                        // ignore: deprecated_member_use
                        child: RaisedButton.icon(
                          onPressed: () {
                            String url = "yandexmaps://maps.yandex.ru/?pt=30.335429,59.944869&z=18&l=map";
                            launch(url);
                          },
                          icon: Icon(
                            Icons.location_on,
                            color: Colors.white, //вид иконки.
                          ),
                          label: Text(
                            'Посмотреть на карте',
                            style: TextStyle(color: Colors.white),
                          ), // название кнопки
                          color: Colors.grey, // цвет кнопки
                        )),
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        height: 50,
                        width: 390,
                        decoration: BoxDecoration(color: Colors.purple, borderRadius: BorderRadius.circular(3)),
                        // ignore: deprecated_member_use
                        child: RaisedButton.icon(
                          onPressed: () async {
                            var basketPrice = await BasketHelper().getTotalCost(orderId);
                            Timer(Duration(seconds: 0), () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PrintPage(
                                            basket: jsonDecode(basket),
                                            basketPrice: basketPrice,
                                          )));
                            });
                          },
                          icon: Icon(
                            Icons.print,
                            color: Colors.white, //вид иконки.
                          ),
                          label: Text(
                            'Печать',
                            style: TextStyle(color: Colors.white),
                          ), // название кнопки
                          color: Colors.blue, // цвет кнопки
                        ))
                  ]));
        });
  }
}
