// ignore: import_of_legacy_library_into_null_safe
import 'dart:async';
import 'dart:convert';

import 'package:dvapp/delivered_orders.dart';
import 'package:dvapp/pages/current_orders.dart';
import 'package:dvapp/system/api.dart';
import 'package:dvapp/system/database.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

class OrderTypePage extends StatefulWidget {
  final fullName;

  const OrderTypePage({Key key, this.fullName}) : super(key: key);

  @override
  _OrderTypePageState createState() => _OrderTypePageState();
}

class _OrderTypePageState extends State<OrderTypePage> {
  final controler = TextEditingController();
  bool loader;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Мои заказы"),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 10),
              child: FutureBuilder(
                future: SharedPreferences.getInstance(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    return Text("Имя водителя - ${snapshot.data.getString('user_full_name')}",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12));
                  }
                  return Container();
                },
              )),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text("Версия 1.0.0", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 60.0),
            child: Center(
              child: Container(
                  width: 200,
                  height: 150,
                  /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                  child: Image.asset('assets/images/logo_2.png')),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 100),
            height: 50,
            width: 250,
            decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.circular(20)),
            // ignore: deprecated_member_use
            child: FlatButton(
              onPressed: () async {
                Timer(Duration(seconds: 0), () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CurrentOrders()));
                });
              },
              child: Text(
                'Текущие',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 30),
            height: 50,
            width: 250,
            decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.circular(20)),
            // ignore: deprecated_member_use
            child: FlatButton(
              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => DeliveredOrders())),
              child: Text(
                'Выполненные',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
          ),
          loader == true
              ? Column(
                  children: [
                    CircularProgressIndicator(),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: Text('Идет загрузка данных ...'),
                    )
                  ],
                )
              : Container(
                  margin: EdgeInsets.only(top: 30),
                  height: 50,
                  width: 250,
                  decoration: BoxDecoration(color: Colors.blue, borderRadius: BorderRadius.circular(20)),
                  // ignore: deprecated_member_use
                  child: FlatButton(
                    onPressed: () async {
                      var orders = await Api().getOrdersByFilter(filter: '');
                      Database db = await DBProvider.db.database;
                      var batch = db.batch();
                      orders['data'].forEach((order) {
                        batch.rawInsert(
                            "INSERT OR REPLACE INTO Orders (id,order_id,basket,store_name,store_id,store_address,total_cost,total_returns_cost,"
                            "counterparty_name,delivery_at,status,bonus_game_sum)"
                            " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                            [
                              order['id'],
                              order['order_id'],
                              jsonEncode(order['basket']),
                              order['store_name'],
                              order['store_id'],
                              order['store_address'],
                              order['total_cost'].toString(),
                              order['total_returns_cost'].toString(),
                              order['counterparty_name'],
                              order['delivery_at'],
                              order['status'],
                              order['bonus_game_sum'],
                            ]);
                      });
                      await batch.commit();

                      var test = await DBProvider.db.allOrders();
                      print(test);
                    },
                    child: Text(
                      'Загрузить',
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                ),
        ]),
      ),
    );
  }
}
