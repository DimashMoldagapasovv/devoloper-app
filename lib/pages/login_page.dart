import 'dart:async';

import 'package:dvapp/pages/order_types_page.dart';
import 'package:dvapp/system/api.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _key = new GlobalKey<FormState>();
  var _autovalidate;
  bool loader;
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Страница авторизаций"),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Form(
            key: _key,
            autovalidateMode: _autovalidate,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 60.0),
                  child: Center(
                    child: Container(
                        margin: EdgeInsets.only(bottom: 70),
                        width: 200,
                        height: 150,
                        /*decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(50.0)),*/
                        child: Image.asset('assets/images/logo_2.png')),
                  ),
                ),
                loader == true
                    ? Column(
                        children: [
                          CircularProgressIndicator(),
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Text('Идет загрузка данных ...'),
                          )
                        ],
                      )
                    : Column(children: [
                        Padding(
                          //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
                          padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            validator: (val) => _validateEmail(val, 'Email'),
                            decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.person), border: OutlineInputBorder(), labelText: 'Логин', hintText: 'Введите E-mail'),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 15, bottom: 0),
                          //padding: EdgeInsets.symmetric(horizontal: 15),
                          child: TextFormField(
                            controller: passwordController,
                            textInputAction: TextInputAction.next,
                            autofocus: true,
                            obscureText: true,
                            decoration: InputDecoration(
                                prefixIcon: const Icon(Icons.vpn_key),
                                border: OutlineInputBorder(),
                                // Navigator.push(context,
                                // MaterialPageRoute(builder: (context) => PagesDemo())),der: OutlineInputBorder(),
                                labelText: 'Пароль',
                                hintText: 'Введите пароль'),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 50),
                          height: 50,
                          width: 250,
                          decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.circular(20)),
                          child: FlatButton(
                            onPressed: () async {
                              await login();
                            },
                            // Navigator.push(context,
                            // MaterialPageRoute(builder: (context) => PagesDemo())),
                            child: Text(
                              'Войти',
                              style: TextStyle(color: Colors.white, fontSize: 25),
                            ),
                          ),
                        ),
                      ]),
                SizedBox(
                  height: 130,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String _validateEmail(String val, fieldMame) {
    if (val == null || val == '') {
      return '$fieldMame обязательно заполнить';
    }
    if (val.length < 1) {
      return '$fieldMame Введите правильный';
    }

    return null;
  }

  login() async {
    var formState = _key.currentState;

    if (formState.validate()) {
      print("success login");
      print(emailController.text);
      print(passwordController.text);

      var response = await Api().login(emailController.text, passwordController.text);

      if (response != null) {
        print("sad coder test 123213 412 124 response");
        print(response);
        print("sad coder test 123213 412 124 response");
        SharedPreferences store = await SharedPreferences.getInstance();
        store.setString("user_full_name", response['user']["full_name"]);
        store.setString("user_token", response["token"]);
        store.setString("user_email", response['user']["email"]);

        Timer(Duration(seconds: 0), () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => OrderTypePage(
                        fullName: response['user']["full_name"],
                      )));
        });
      }
    } else {
      print("login failder");
    }
  }
}
