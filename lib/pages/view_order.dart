import 'dart:async';
import 'dart:convert';

import 'package:dvapp/helpers/basket_helper.dart';
import 'package:dvapp/modals/change_product_count.dart';
import 'package:dvapp/pages/order_types_page.dart';
import 'package:dvapp/system/api.dart';
import 'package:dvapp/system/database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ViewOrder extends StatefulWidget {
  // Generate a dummy list
  final basket;
  final orderId;
  final storeId;
  final title;
  final basketCost;

  const ViewOrder({Key key, this.basket, this.orderId, this.storeId, this.title, this.basketCost}) : super(key: key);

  @override
  _ViewOrderState createState() => _ViewOrderState();
}

class _ViewOrderState extends State<ViewOrder> {
  List<dynamic> listViewItems = [];
  List<dynamic> testList = [];

  createAlertDialog(BuildContext context) {
    TextEditingController customController = TextEditingController();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Ваши следующие действия"),
            content: TextField(
              decoration: InputDecoration(
                hintText: ("Укажите число"),
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                prefixIcon: const Icon(Icons.adjust_sharp),
              ),
              controller: customController,
            ),
            actions: <Widget>[
              MaterialButton(
                elevation: 5.0,
                child: Text(
                  "Изменить",
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: () {},
              )
            ],
          );
        });
  }

  createAlertOrderAcsess(BuildContext context) {
    TextEditingController customController = TextEditingController();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Вы действительно хотите выполнить заказ выберите тип оплаты?'),
            actions: <Widget>[
              Column(
                children: [
                  Container(
                    width: 250,
                    height: 65,
                    child: TextField(
                      maxLength: 11,
                      controller: customController,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.person),
                          border: OutlineInputBorder(),
                          labelText: 'Номер телефона',
                          hintText: 'Введите '
                              'каспи номер'),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Row(
                      children: [
                        FlatButton(
                          padding: EdgeInsets.only(right: 10, left: 10),
                          color: Colors.green,
                          child: Text(
                            "Да наличка",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () async {
                            await Api().updateOrders(orderId: this.widget.orderId, storeId: this.widget.storeId);
                            await Api().updateOrderStatus(this.widget.orderId, 3);
                            SharedPreferences storage = await SharedPreferences.getInstance();
                            String priceOrder = await BasketHelper().getTotalCost(this.widget.orderId);
                            storage.setString("nal:${this.widget.orderId}", priceOrder.toString());
                            await DBProvider.db.updateOrderStatus(id: this.widget.orderId, status: 3);

                            Navigator.of(context).pop();
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => OrderTypePage()));
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: FlatButton(
                            padding: EdgeInsets.only(right: 10, left: 10),
                            color: Colors.green,
                            child: Text(
                              "Да без нал каспи",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () async {
                              await Api().updateOrders(orderId: this.widget.orderId, storeId: this.widget.storeId);
                              await Api().updateOrderStatus(this.widget.orderId, 3);
                              String priceOrder = await BasketHelper().getTotalCost(this.widget.orderId);
                              SharedPreferences storage = await SharedPreferences.getInstance();
                              storage.setString("beznal:${this.widget.orderId}", priceOrder.toString());

                              var orderCost = await BasketHelper().getTotalCost(this.widget.orderId);

                              await Api().sendBezNal(orderCost, customController.text.toString());

                              await DBProvider.db.updateOrderStatus(id: this.widget.orderId, status: 3);

                              Navigator.of(context).pop();
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => OrderTypePage()));
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: FlatButton(
                            padding: EdgeInsets.only(right: 1),
                            color: Colors.red,
                            child: Text(
                              "Нет",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              //Put your code here which you want to execute on No button click.
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: FlatButton(
                            color: Colors.grey,
                            child: Text(
                              "Назад",
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () {
                              //Put your code here which you want to execute on Cancel button click.
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          );
        });
  }

  createAlertDel(BuildContext context) {
    TextEditingController customController = TextEditingController();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Вы действительно хотите удалить ?'),
            actions: <Widget>[
              FlatButton(
                padding: EdgeInsets.only(right: 1),
                color: Colors.green,
                child: Text(
                  "Да",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  //Put your code here which you want to execute on Yes button click.
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                padding: EdgeInsets.only(right: 1),
                color: Colors.red,
                child: Text(
                  "Нет",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  //Put your code here which you want to execute on No button click.
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                color: Colors.grey,
                child: Text(
                  "Назад",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  //Put your code here which you want to execute on Cancel button click.
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  createAlertRenoun(BuildContext context) {
    TextEditingController customController = TextEditingController();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Вы действительно хотите отменить заказ?'),
            actions: <Widget>[
              FlatButton(
                padding: EdgeInsets.only(right: 1),
                color: Colors.green,
                child: Text(
                  "Да",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  //Put your code here which you want to execute on Yes button click.
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                padding: EdgeInsets.only(right: 1),
                color: Colors.red,
                child: Text(
                  "Нет",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  //Put your code here which you want to execute on No button click.
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                color: Colors.grey,
                child: Text(
                  "Назад",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  //Put your code here which you want to execute on Cancel button click.
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  void initState() {
    this.listViewItems = jsonDecode(this.widget.basket);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          centerTitle: true,
          title: Text(
            this.widget.title == null ? 'Выдача заказа' : this.widget.title,
          ),
        ),
        body: Container(
          margin: EdgeInsets.all(20),
          // Use ListView.builder
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                    // the number of items in the list
                    itemCount: listViewItems.length,
                    shrinkWrap: true,
                    // display each item of the product list
                    itemBuilder: (context, index) {
                      if (listViewItems[index]['type'] != null) {
                        Color textColor = listViewItems[index]['type'] == 0 ? Colors.black : Colors.red;
                        return GestureDetector(
                          onTap: () async {
                            if (this.widget.orderId != null) {
                              Timer(Duration(seconds: 0), () {
                                Navigator.of(context).push(PageRouteBuilder(
                                    opaque: false,
                                    pageBuilder: (BuildContext context, _, __) => ChangeProductCount(
                                          orderId: this.widget.orderId,
                                          productId: listViewItems[index]['product_id'],
                                        )));
                              });
                            }
                          },
                          child: Container(
                              child: Column(children: [
                            Container(
                              child: Text(
                                "${listViewItems[index]['name_1c']}",
                                style: TextStyle(color: textColor),
                              ),
                              margin: EdgeInsets.only(bottom: 5),
                            ),
                            Row(
                              children: [
                                Text(
                                  'Колличество: ',
                                  style: TextStyle(color: textColor),
                                ),
                                // Icon(Icons.pie_chart_outline_outlined),
                                Expanded(
                                  child: Container(
                                    child: Text(
                                      "${listViewItems[index]['count']}",
                                      style: TextStyle(color: textColor),
                                    ),
                                  ),
                                  flex: 1,
                                ),
                                Text(
                                  "Сумма: ${listViewItems[index]['all_price'].toStringAsFixed(2)}",
                                  style: TextStyle(color: textColor),
                                ),
                              ],
                            ),
                            Divider(
                              color: Colors.red,
                              thickness: 3,
                            ),
                          ])),
                        );
                      }
                      return GestureDetector(
                        onTap: () {
                          _showOrderActions(context);
                        },
                        child: Column(children: [
                          Container(
                            child: Text("${listViewItems[index]['name_1c']}"),
                            margin: EdgeInsets.only(bottom: 15),
                          ),
                          Row(
                            children: [
                              Text('Колличество: '),
                              // Icon(Icons.pie_chart_outline_outlined),
                              Expanded(
                                child: Container(
                                  child: Text("1"),
                                ),
                                flex: 2,
                              ),
                              Text('Сумма: '),
                              Expanded(
                                child: Container(
                                  child: Text("${listViewItems[index]['all_price']}"),
                                ),
                                flex: 1,
                              )
                            ],
                          ),
                          Divider(
                            thickness: 2,
                            color: Colors.grey,
                          )
                        ]),
                      );
                    }),
              ),
              this.widget.orderId != null
                  ? Row(children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width * 0.33,
                        height: 50,
                        margin: EdgeInsets.only(bottom: 5, top: 10),
                        child: Center(
                          // ignore: deprecated_member_use
                          child: FlatButton.icon(
                            height: 50,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
                            color: Colors.green,
                            textColor: Colors.white,
                            onPressed: () {
                              createAlertOrderAcsess(context);
                            },
                            icon: Icon(Icons.send),
                            label: Text('Выполнить'),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.25,
                        height: 50,
                        margin: EdgeInsets.only(bottom: 5, top: 10, left: 1),
                        child: Center(
                          // ignore: deprecated_member_use
                          child: FlatButton.icon(
                            minWidth: 200,
                            height: 50,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
                            color: Colors.red,
                            textColor: Colors.white,
                            onPressed: () {
                              createAlertRenoun(context);
                            },
                            icon: Icon(Icons.not_interested_rounded),
                            label: Text('Отказ'),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: 50,
                        margin: EdgeInsets.only(bottom: 5, top: 10, left: 1),
                        child: Center(
                          // ignore: deprecated_member_use
                          child: FlatButton.icon(
                            minWidth: 200,
                            height: 50,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
                            color: Colors.blueAccent,
                            textColor: Colors.white,
                            onPressed: () async {
                              var barCode = await scanBarcodeNormal();
                              print("sad coder 123213 barCode");
                              print(barCode);
                            },
                            icon: Icon(Icons.add_circle_outlined),
                            label: Text('Добавить'),
                          ),
                        ),
                      ),
                    ])
                  : Container(),
              Center(
                  child: Container(
                margin: EdgeInsets.only(top: 5),
                child: FutureBuilder(
                  future: BasketHelper().getTotalCost(this.widget.orderId),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return Text("Общая сумма заказа: ${snapshot.data}");
                    }
                    return Container();
                  },
                ),
              )),
              Center(
                  child: Container(
                margin: EdgeInsets.only(top: 10),
                child: FutureBuilder(
                  future: BasketHelper().getTotalCount(this.widget.orderId),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return Text("Общая колличество товаров: ${snapshot.data}");
                    }
                    return Container();
                  },
                ),
              ))
            ],
          ),
        ));
  }

  _showOrderActions(context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 290,
              child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(
                          'Что вы хотите сделать?',
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 20.0,
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.only(bottom: 13, top: 30),
                      child: Center(
                        // ignore: deprecated_member_use
                        child: FlatButton(
                          minWidth: 390,
                          height: 50,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                          color: Colors.red,
                          textColor: Colors.white,
                          onPressed: () {
                            createAlertDel(context);
                          },
                          child: Text(
                            "Удалить",
                            style: TextStyle(fontSize: 14.0),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      child: Center(
                        // ignore: deprecated_member_use
                        child: FlatButton(
                          minWidth: 390,
                          height: 50,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                          color: Colors.green,
                          textColor: Colors.white,
                          onPressed: () {
                            createAlertDialog(context);
                          },
                          child: Text(
                            "Изменить колличество",
                            style: TextStyle(fontSize: 14.0),
                          ),
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        height: 50,
                        width: 390,
                        child: RaisedButton.icon(
                          onPressed: () {},
                          icon: Icon(
                            Icons.wifi_protected_setup_outlined,
                            color: Colors.white, //вид иконки.
                          ),
                          label: Text(
                            'Редеактировать',
                            style: TextStyle(color: Colors.white),
                          ), // название кнопки
                          color: Colors.grey, // цвет кнопки
                        ))
                  ]));
        });
  }

  Future scanBarcodeNormal() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode("#ff6666", "Cancel", true, ScanMode.BARCODE);
      return int.parse(barcodeScanRes);
    } on PlatformException {
      return null;
    }

    if (!mounted) return;

    print("sad coder 12312414barcodeScanRes");
    print(barcodeScanRes);
    print("sad coder 12312414barcodeScanRes");
  }
}
